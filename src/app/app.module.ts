import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';



import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { JumbotronComponent } from './home/jumbotron/jumbotron.component';
import { ProductCategoriesComponent } from './home/product-categories/product-categories.component';
import { FooterComponent } from './footer/footer.component';
import { ProdutsComponent } from './produts/produts.component';
import { QuickSearchComponent } from './produts/quick-search/quick-search.component';
import { CategoriesComponent } from './produts/categories/categories.component';
import { ProductGridComponent } from './produts/product-grid/product-grid.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    JumbotronComponent,
    ProductCategoriesComponent,
    FooterComponent,
    ProdutsComponent,
    QuickSearchComponent,
    CategoriesComponent,
    ProductGridComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
